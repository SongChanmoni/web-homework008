import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import MyMenu from './Components/MyMenu';
import MyForm from './Components/MyForm';
import MyTable from './Components/MyTable';
import { Container, Row, Col, } from 'react-bootstrap';

export default class App extends Component {


  constructor(props) {
    super(props);
    this.state = {
      userAcc: {
        username: "",
        email: "",
        password: "",
        gender: "",
        status: true,
        isActive: null,
      },
      account: [

      ],
    };
  }

  onHandleText = (e) => {
    let temp = { ...this.state.userAcc };
    let name = e.target.value
    let check = e.target.name
    if (check === "username")
      temp.username = name
    else if (check === "email")
      temp.email = name
    else if (check === "password")
      temp.password = name
    else
      temp.gender = name
    this.setState({
      userAcc: temp
    })

    this.setState({ userAcc: temp });
    console.log("TempAcc", this.state.userAcc);
  };
  onSave = () => {
    let temp = [...this.state.account]
    let items = { ...this.state.userAcc }
    temp.push(items)
    this.setState({
      account: temp
    })
  };

  onSelect = index => {
    let temp = [...this.state.account]
    if (index === temp[index].isActive) {
      temp[index].isActive = null
    } else {
      temp[index].isActive = index
    }
    this.setState({
      account: temp
    })
    console.log(this.state.account);
  };

  onDelete = () => {
    console.log("account", this.state.account);
    let tmp=this.state.account.filter(item=>{
      return item.isActive===null
    })
    
    this.setState({
      account: tmp
    })
   
    console.log("delect", this.state.account);
  }


  onvalida = (e) => {
    console.log("e:", e.target);
    //let name = "email"
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        console.log(this.state);

        if (e.target.name === "email") {
          //apple1212@#@gmail.com
          let pattern = /^\S+@\S+\.[a-z]{3}$/g;
          let result = pattern.test(this.state.email.trim());

          if (result) {
            this.setState({
              emailErr: "",
            });
          } else if (this.state.email === "") {
            this.setState({
              emailErr: "Email cannot be empty!",
            });
          } else {
            this.setState({
              emailErr: "Email is invalid!",
            });
          }
        }

        if (e.target.name === "password") {
          let pattern2 =
            /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[_\W])([a-zA-Z0-9_\W]{8,})$/g;
          let result2 = pattern2.test(this.state.password);

          if (result2) {
            this.setState({
              passwordErr: "",
            });
          } else if (this.state.password === "") {
            this.setState({
              passwordErr: "Password cannot be empty!",
            });
          } else {
            this.setState({
              passwordErr: "Password is invalid!",
            });
          }
        }
      }
    );
  };



  render() {
    return (
      <div>
        <MyMenu />
        <Container>
          <Row>
            <Col ms="5">
              <MyForm items={this.state.account}
                onHandleText={this.onHandleText}
                onSave={this.onSave} />
            </Col>

            <Col ms="5">
              <MyTable items={this.state.account}
                onSelect={this.onSelect} onDeletes={this.onDelete}
              />
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}




