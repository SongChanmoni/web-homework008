import React, { Component } from 'react'
import { Form, Button, Image, type, Type, Col } from 'react-bootstrap'

export default function MyForm(props) {


   
   
        return (
            <div>
                <Col>
                    <Form style={{ width: "400px" }} >
                        <Image style={{ marginLeft: "150px" }}  width="100px" height="100" src="https://www.shareicon.net/data/512x512/2016/08/05/806962_user_512x512.png" />
                        <h2 style={{ textAlign: "center",color:"blue" }}>User Account</h2>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label><strong>Username</strong></Form.Label>
                            <Form.Control   onChange={(e) => props.onHandleText(e)} name="username"  type="text" placeholder="username" />
                            <Form.Text className="text-muted">
                            </Form.Text>
                        </Form.Group>
                        <Form.Label><strong>Gender</strong></Form.Label>
                        <br />
                        <Form.Check style={{ marginLeft: "50px" }}
                              onChange={(e) => props.onHandleText(e)}
                            custome
                            inline
                            label="Male"
                            type="radio"
                            value="male"
                            name="gender"
                        />
                        <Form.Check style={{ marginLeft: "50px" }}
                              onChange={(e) => props.onHandleText(e)}
                            custome
                            inline
                            label="Female"
                            type="radio"
                            value="female"
                            name="gender"
                        />
                        <Form.Group controlId="formBasicEmail" style={{ marginTop: "10px" }}>
                            <Form.Label><strong>Email</strong></Form.Label>
                            <Form.Control    onChange={(e) => props.onHandleText(e)} name="email" placeholder="email" />
                            <Form.Text className="text-muted">
                            </Form.Text>

                        </Form.Group>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label><strong>Password</strong></Form.Label>
                            <Form.Control   onChange={(e) => props.onHandleText(e)} type="password" name="password" placeholder="password" />
                            <Form.Text className="text-muted">
                            </Form.Text>
                        </Form.Group>

                        <Button type="button" value="Submit"  onClick={props.onSave}>
                            Save
                        </Button>
                    </Form>
                </Col>

            </div>
        )
  
}
