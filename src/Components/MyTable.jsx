import React, { Component } from 'react'
import { Table, Button } from 'react-bootstrap'

export default function MyTable({ items,  onSelect, onDeletes }) {

    return (
        <div>
            <Table striped bordered hover style={{ textAlign: "center", marginTop: "170px", width: "650px" }} >
                <thead>
                    <tr>
                        <th>#</th>
                        <th>UserName</th>
                        <th>Email</th>
                        <th>Gender</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        items.map((item, index) => (
                            <tr style={ item.isActive === index ? { background: 'gray' }: { background: 'white' }}
                                key={index}
                                onClick={() => onSelect(index)}  >
                                <td>{index + 1}</td>
                                <td>{item.username}</td>
                                <td>{item.email}</td>
                                <td>{item.gender}</td>
                            </tr>
                        ))
                    }
                </tbody>
            </Table>
            <Button onClick={onDeletes} variant="danger" style={{ marginTop: "5px" }}>Delete</Button>

        </div>
    )

}
